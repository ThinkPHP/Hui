### Hui是一款移动UI框架，兼容Android 4.0+，IOS7+，Chrome，FF，Opera，IE10+。


### Hui 开发者信息

*****

* 框架名称：`Hui`
* 框架作者：`新生帝`
* 作者Q Q：`8020292`
* QQ交流群：`18863883`，加群请注明：`Hui`
* 开发日期：`2016年03月13日`
* 版权所有：`中山赢友网络科技有限公司`
* 企业官网：[http://www.winu.net](http://www.winu.net)
* 开源协议：`GPL v2 License`
* 系统描述：`一切从简，只为了更懒！`

*****

### Hui更新记录：

```
 ### 2016年03月21日 V1.4

* [新增] 仿微信通讯录
* [新增] Hui.js
* [新增] 联系人列表
* [新增] 手势解锁
* [优化] H.js
* [优化] 大量Hui的样式
* [优化] 界面加载性能
* [优化] 字体图标所有命名

 **************************

 ### 2016年03月20日 V1.3

* [新增] 版本提示
* [新增] 版本更新记录
* [新增] 下拉刷新、上拉加载
* [新增] 加载特效
* [新增] 左右侧滑页面
* [新增] 仿微信通讯录
* [新增] 功能菜单
* [新增] 分享菜单
* [新增] 提示框
* [新增] 时间轴卡片
 *[新增] 大量常用样式
 *[新增] 字体图标
 *[优化] 顶部导航栏

 **************************

 ### 2016年03月19日 V1.2

* [新增] 字体图标
* [新增] 大量常用样式
* [新增] 底部导航栏带角标
* [新增] 带搜索的头部，并附加主题
* [优化] 按钮样式
* [优化] 大量样式命名、注释
 *[优化] 聊天组件

 **************************

 ```

### Hui公测版本：

* IOS：[IOS下载](http://downloadpkg.apicloud.com/app/download?path=http://7xrvzc.com1.z0.glb.clouddn.com/5f62304414d5c3d9ce8614d8f293e868.ipa)
* Android：[Android下载](http://downloadpkg.apicloud.com/app/download?path=http://7xrvzc.com1.z0.glb.clouddn.com/465e5122bcdc972a85d6b16c1bdaf4f0_d)

*****

### Hui 友情捐赠

*****

如果你觉得 Hui 对你有价值，并且愿意让她继续成长下去，你可以资助这个项目的开发，为作者加油打气。

![友情捐赠Hui](http://git.oschina.net/uploads/images/2016/0207/160936_8f2d5f2e_526496.png "友情捐赠Hui")

*****

如果你喜欢Hui，可以点击右上角的`star`，想实时关注进度，可以点击右上角的`watch`。

最后，感谢每一个提建议和使用或者捐赠的朋友！因为你们，我们才能坚持！也是因为你们，未来才会更美好！

### Hui部分效果图：

![输入图片说明](http://git.oschina.net/uploads/images/2016/0321/231819_b6915aab_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0321/231828_583805c3_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0321/231837_b80e4b39_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0320/025558_e65e7d17_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0320/025551_0f2a8610_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0320/025543_3e5f94ba_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0320/025535_75642229_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0320/025528_999fdfc4_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0320/025521_28e79a4a_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0320/025459_f1b05894_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0319/153521_ae914ebb_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0319/153531_799ff6ad_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0319/153539_ab7546de_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/174955_c98adeb9_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175005_0baaeac3_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175136_64807e4c_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175144_3d791069_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175151_7f5ba293_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175201_45a51e25_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175210_b57274a5_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175218_6deedf79_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175014_cb0bd186_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175022_9ad06f2b_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175031_b003adbb_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175040_73a87050_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175048_4de53f0c_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175054_ae027191_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175101_7bdff864_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175107_80d4b781_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175114_24a9b90e_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175122_ea4027dc_526496.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0318/175128_79c6f069_526496.png "在这里输入图片标题")